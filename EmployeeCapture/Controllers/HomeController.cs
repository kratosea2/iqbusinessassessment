﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EmployeeCapture.Data;
using EmployeeCapture.Models;
using EmployeeCapture.Data.Repositories;


namespace EmployeeCapture.Controllers
{
    public class HomeController : Controller
    {
        private EmployeeRepository employeeRepository = new EmployeeRepository();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ViewExistingEmployees()
        {
            List<EmployeeViewModel> employeesToView = new List<EmployeeViewModel>();
            var employees = employeeRepository.GetAllEmployees();
            foreach (var employee in employees)
            {
                employeesToView.Add(new EmployeeViewModel
                {
                    Id = employee.EmployeeId,
                    FirstName = employee.FirstName,
                    Surname = employee.LastName,
                    IdNumber = employee.IdNumber,
                    ContactNumber = employee.ContactNumber,
                    Address = employee.Address,
                    Email = employee.Email,
                    Gender = employee.Gender,
                    FacebookProfileLink = employee.FacebookProfile,
                    LinkedInProfileLink = employee.LinkedInProfile,
                    TwitterProfileLink = employee.TwitterProfile,
                    DateAdded = employee.AddedDate
                });

            }
            return View("Employees", employeesToView);
        }

        public ActionResult Create()
        {
            return View(new EmployeeViewModel());
        }

        public ActionResult Details(int id)
        {
            var customerToView = employeeRepository.GetEmployee(id);

            if (customerToView == null)
                return RedirectToAction("Index");
            var employeeView = new EmployeeViewModel
            {
                FirstName = customerToView.FirstName,
                Surname = customerToView.LastName,
                LinkedInProfileLink = customerToView.LinkedInProfile,
                FacebookProfileLink = customerToView.FacebookProfile,
                TwitterProfileLink = customerToView.TwitterProfile,
                Gender = customerToView.Gender,
                ContactNumber = customerToView.ContactNumber,
                Email = customerToView.Email,
                Address = customerToView.Address,
                DateAdded = customerToView.AddedDate,
                IdNumber = customerToView.IdNumber
            };
            return View(employeeView);
        }

        [HttpPost]
        public ActionResult Create(EmployeeViewModel employeeDetails)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    Response.StatusCode = (int)HttpStatusCode.BadRequest;
                    return Json(new { success = false, responseMessage = "Some values are still required"});
                }

                employeeRepository.SaveNewEmployee(employeeDetails);

                return Json(new { success = true, responseMessage = "Employee was successfully added" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = ex.InnerException.Message });
            }
        }

        public ActionResult Edit(int id)
        {
            var employeeToEdit = employeeRepository.GetEmployee(id);
            Session["editUserId"] = id;

            var employeeView = new EmployeeViewModel
            {
                FirstName = employeeToEdit.FirstName,
                Surname = employeeToEdit.LastName,
                LinkedInProfileLink = employeeToEdit.LinkedInProfile,
                FacebookProfileLink = employeeToEdit.FacebookProfile,
                TwitterProfileLink = employeeToEdit.TwitterProfile,
                Gender = employeeToEdit.Gender,
                ContactNumber = employeeToEdit.ContactNumber,
                Email = employeeToEdit.Email,
                Address = employeeToEdit.Address,
                IdNumber = employeeToEdit.IdNumber,
                DateAdded = employeeToEdit.AddedDate,
            };

            return View(employeeView);
        }

        [HttpPost]
        public ActionResult Edit(EmployeeViewModel employeeToEdit)
        {
            try
            {
                var emplloyeeId = (int)Session["editUserId"];
                var originalCustomerDetail = employeeRepository.GetEmployee(emplloyeeId);
                
                employeeRepository.UpdateEmployee(employeeToEdit, emplloyeeId);

                return Json(new { success = true, responseMessage = $"Record for {employeeToEdit.FirstName} {employeeToEdit.Surname}" 
                    + $" was updated successfully" });
            }
            catch (Exception ex)
            {
                return Json(new { success = false, responseMessage = ex.InnerException.Message });
            }
        }
    }
}
