﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EmployeeCapture.Employee;
using EmployeeCapture.Models;

namespace EmployeeCapture.Data.Repositories
{
    public class EmployeeRepository : EmployeeRepositoryBase, IEmployee
    {
        public EmployeeRepository() { }

        public void SaveNewEmployee(EmployeeViewModel employeeDetails)
        {
            var employeeToAdd = new Employee()
            {
                FirstName = employeeDetails.FirstName,
                LastName = employeeDetails.Surname,
                IdNumber = employeeDetails.IdNumber,
                ContactNumber = employeeDetails.ContactNumber,
                Address = employeeDetails.Address,
                Email = employeeDetails.Email,
                Gender = employeeDetails.Gender,
                FacebookProfile = employeeDetails.FacebookProfileLink,
                LinkedInProfile = employeeDetails.LinkedInProfileLink,
                TwitterProfile = employeeDetails.TwitterProfileLink,
                AddedDate = DateTime.UtcNow
            };

            save(employeeToAdd);
        }

        public void UpdateEmployee(EmployeeViewModel employeeDetails, long id)
        {
            var employeeToUpdate = Db.Employees.SingleOrDefault(x => x.EmployeeId == id);

            if (employeeToUpdate != null)
            {
                employeeToUpdate.FirstName = employeeDetails.FirstName;
                employeeToUpdate.LastName = employeeDetails.Surname;
                employeeToUpdate.LinkedInProfile = employeeDetails.LinkedInProfileLink;
                employeeToUpdate.FacebookProfile = employeeDetails.FacebookProfileLink;
                employeeToUpdate.TwitterProfile = employeeDetails.TwitterProfileLink;
                employeeToUpdate.ContactNumber = employeeDetails.ContactNumber;
                employeeToUpdate.Email = employeeDetails.Email;
                employeeToUpdate.Address = employeeDetails.Address;;

                save();
            }
        }

        public Employee GetEmployee(long id)
        {
            return Db.Employees.FirstOrDefault(x => x.EmployeeId == id);
        }

        public List<Employee> GetAllEmployees()
        {
            return Db.Employees.ToList();
        }

        public void save()
        {
            Db.SaveChanges();
        }

        public void save(Employee employee)
        {
            Db.Employees.Add(employee);
            Db.SaveChanges();
        }
    }
}