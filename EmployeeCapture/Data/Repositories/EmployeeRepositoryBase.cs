﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EmployeeCapture.Data;

namespace EmployeeCapture.Data.Repositories
{
    public abstract class EmployeeRepositoryBase : IDisposable
    {
        protected EmployeeRepositoryBase()
        {
            this.Db = new EmployeeDetailsEntities();
        }

        protected EmployeeDetailsEntities Db
        {
            get; set;
        }

        protected bool IsDisposed
        {
            get;
            set;
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (isDisposing)
            {
                Db.Dispose();
                this.IsDisposed = true;
            }
        }

        public void Dispose()
        {
            this.Dispose(true);
        }
    }
}