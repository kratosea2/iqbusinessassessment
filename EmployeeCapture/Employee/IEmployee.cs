﻿using System.Collections.Generic;
using EmployeeCapture.Data;
using EmployeeCapture.Models;

namespace EmployeeCapture.Employee
{
    public interface IEmployee
    {
        void SaveNewEmployee(EmployeeViewModel employeeDetails);
        void UpdateEmployee(EmployeeViewModel employeeDetails, long id);
        Data.Employee GetEmployee(long id);
        List<Data.Employee> GetAllEmployees();
    }
}
