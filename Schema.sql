﻿USE [IQ2019]

CREATE TABLE [dbo].[Employee](
	[EmployeeId] [bigint] NOT NULL IDENTITY PRIMARY KEY,
	[FirstName] [nvarchar](300) NOT NULL,
	[LastName] [nvarchar](300) NOT NULL,
	[Address] [nvarchar](500) NULL,
	[Email] [nvarchar](200) NOT NULL,
	[ContactNumber] [varchar](20) NULL,
	[AddedDate] [datetime] NOT NULL,
	[FacebookProfile] [nvarchar](500) NULL,
	[TwitterProfile] [nvarchar](500) NULL,
	[LinkedInProfile] [nvarchar](500) NULL,
	[Gender] [nvarchar] (6) NOT NULL,
	[IdNumber] [nvarchar] (100) NOT NULL
)