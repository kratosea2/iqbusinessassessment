﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EmployeeCapture.Data.Repositories;
using EmployeeCapture.Data;
using Moq;
using EmployeeCapture.Test.Helpers;
using Autofac.Extras.Moq;

namespace EmployeeCapture.Test
{
    [TestClass]
    public class EmployeeTest
    {
        [TestMethod]
        public void AddNewEmployee()
        {
            var newEmployee = new EmployeeViewModel();

            using (var mock = AutoMock.GetLoose())
            {
                var _repoMock = mock.Mock<EmployeeRepository>();
                _repoMock.Setup(x => x.SaveNewEmployee(newEmployee.GetEmployeeInformation()));

                _repoMock.Verify(x => x.SaveNewEmployee(newEmployee.GetEmployeeInformation()), Times.Once());
            }
        }
    }
}
