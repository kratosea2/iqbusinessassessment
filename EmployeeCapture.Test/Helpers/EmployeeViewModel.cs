﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EmployeeCapture.Test.Helpers
{
    public class EmployeeViewModel
    {
        [Display(Name = "First Name")]
        public long Id { get; set; }

        [Required]
        [Display(Name = "First Name")]
        [StringLength(300, MinimumLength = 2, ErrorMessage = "Name entered is too short")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Surname")]
        [StringLength(300, MinimumLength = 2, ErrorMessage = "Surname entered is too short")]
        public string Surname { get; set; }

        [Required]
        [Display(Name = "Id Number")]
        [StringLength(100, ErrorMessage = "Id Number entered was too long")]
        public string IdNumber { get; set; }

        [Required]
        [Display(Name = "Select your gender")]
        public string Gender { get; set; }

        [Display(Name = "Address")]
        [DataType(DataType.MultilineText)]
        [StringLength(500, ErrorMessage = "The address entered is too long")]
        public string Address { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        [StringLength(200, MinimumLength = 3, ErrorMessage = "Invalid email address entered")]
        public string Email { get; set; }

        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Contact Number")]
        public string ContactNumber { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        [Display(Name = "Date Added")]
        public DateTime DateAdded { get; set; }

        [Display(Name = "Facebook Profile")]
        [StringLength(500, ErrorMessage = "Link entered was too long")]
        public string FacebookProfileLink { get; set; }

        [Display(Name = "Twitter Profile")]
        [StringLength(500, ErrorMessage = "Link entered was too long")]
        public string TwitterProfileLink { get; set; }

        [Display(Name = "LinkedIn Profile")]
        [StringLength(500, ErrorMessage = "Link entered was too long")]
        public string LinkedInProfileLink { get; set; }

        public List<SelectListItem> Genders { get; } = new List<SelectListItem>()
        {
            new SelectListItem() { Text = "Male", Value = "Male"},
            new SelectListItem() { Text = "Female", Value = "Female"}
        };

        public EmployeeViewModel() { }

        public Models.EmployeeViewModel GetEmployeeInformation()
        {
            return new Models.EmployeeViewModel
            {
                Id = new Random().Next (50, 100),
                FirstName = "TestName",
                Surname = "TestSurname",
                Address = "1 Test Str, Visual Studio",
                Email = "test@visual.com",
                ContactNumber = "000000001",
                Gender = "Female",
                IdNumber = "TestTest",
                DateAdded = DateTime.UtcNow,
                FacebookProfileLink = "http://www.facebook.com/test",
                LinkedInProfileLink = "http://www.linkedin.com/test",
                TwitterProfileLink = "http://www.twitter.com"
            };
        }
    }
}